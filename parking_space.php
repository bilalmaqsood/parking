<?php include 'main.php';?>
<?php include 'functions.php';?>
<!DOCTYPE HTML>
<html>
<head>
<script src="mootools-core.js" type="text/javascript"></script>
	<script src="Source/jquery.min.js" type="text/javascript"></script>
	<script src="mootools-more.js" type="text/javascript"></script>
	<script src="Source/Locale.en-US.DatePicker.js" type="text/javascript"></script>
	<script src="Source/Picker.js" type="text/javascript"></script>
	<script src="Source/Picker.Attach.js" type="text/javascript"></script>
	<script src="Source/Picker.Date.js" type="text/javascript"></script>


<link rel="stylesheet" type="text/css" href="inc/css/structure.css">
<link href="style.css" rel="stylesheet" />
	<link href="Source/datepicker_bootstrap/datepicker_bootstrap.css" rel="stylesheet">
<script>

	// window.addEvent('domready', function(){
	// 	new Picker.Date($$('----'), {
	// 		timePicker: true,
	// 		positionOffset: {x: 5, y: 0},
	// 		pickerClass: 'datepicker_bootstrap',
	// 		useFadeInOut: !Browser.ie
	// 	});
	// });

	window.addEvent('domready', function(){
		new Picker.Date($$('.date'), {
			timePicker: true,
			positionOffset: {x: 5, y: 0},
			pickerClass: 'datepicker_bootstrap',
			useFadeInOut: !Browser.ie
		});
	});



	</script>
</head>
<body>
<form class="box login" action="process-book-2.php" method="post">
	<fieldset class="boxBody">
	<label><strong>Parking Details</strong></label>
	<hr />
	   <label>Recommended Region For You - as per your vehicles body size</label>
	   <select name="street" id="street" class="cjComboBox" >
	   <?php foreach($street as $key => $value){
		   	echo "<option value='".$key."'>".$value."</option>"; 
	   } ?>
		</select>
		
		<select name="plot" id="plot" class="cjComboBox" >
		<?php foreach($plot as $key => $value){
		   	echo "<option value='".$value."'>".$value."</option>"; 
	   } ?>
		</select>
		
	<label>Plate Number</label>
	  <input type="text" tabindex="3" name="plateno" placeholder="eg. KAC 123" required>
	</fieldset>
	<fieldset class="boxBody">
	 <label>Specify Date and time to book</label>
	 <label>From:</label>
	<input type="text" name="from" id="from" class="date" placeholder="02.11.2014 12:05AM">
	<label>To:</label>
	<input type="text" name="to" id="to" placeholder="02.11.2014 12:05AM" class="date">
	  <label>Amount to be charged: Kes. 60/-</label>
	</fieldset>
	<footer>
	  <input type="submit" class="btnLogin btnsubmit" value="Proceed to Date & Time" tabindex="4">
	</footer>
</form>
<script type="text/javascript">
	
$(document).ready(function(){

$(".btnsubmit").on("click",function(e){

e.preventDefault();

var start = $("#from").val();
var end = $("#to").val();
var street =$("#street :selected").val();
var plot =$("#plot :selected").val();
$.ajax({
	url: 'check_booking.php',
	type: 'POST',
	dataType: 'json',
	data: {start: start, end: end,street: street, plot: plot },
})
.done(function(data) {
	
	if(data.status){
		$("form")[0].submit();

	}
	else{
		alert("Space not available please chose other location");
		// console.log("Not done");
	}
})
.fail(function() {
	console.log("err");
})
.always(function() {
	console.log("complete");
});

// alert("Say hello");

});

});


</script>
</body>
</html>
