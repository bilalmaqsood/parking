<?php

	session_start();

    $email = $_SESSION["email"];
    $name = $_SESSION["name"] ;
    $model = $_SESSION["model"]; 
    $vehicle = $_SESSION["vehicle"];
    $street = $_SESSION["street"];
    $plot = $_SESSION["plot"];
    $plateno = $_SESSION["plateno"];
    $account = $_SESSION["account"]; 
    $from = $_SESSION["from"];
    $to = $_SESSION["to"];
	$_SESSION["transaction_id"];
	$amountpaid = isset($_SESSION["transaction_amount"])?$_SESSION["transaction_amount"]:null;


// require('inc\dbcon.php');
require_once ('vendor/autoload.php');
use Dompdf\Dompdf;
$dompdf = new Dompdf();


$html="<html><head>";

	$html.="<link rel='stylesheet' type='text/css' href='css/style-invoice.css' />";
	$html.="<link rel='stylesheet' type='text/css' href='css/print.css' media='print' />";
$html.="</head>";
$html.="<body>";
	$html.="<div id='page-wrap'>";
		$html.="<h1 style='text-align: center;'> Parking Bill </h1>";
		$html.="<div id='identity'>";
            $html.="<table style='border: 0px;' >";
			$html.="<tr><td style='border: 0px;'><b>Name: </b>$name</td></tr>";
			$html.="<tr> <td style='border: 0px;'> <b>Email: </b>$email </td> </tr>";
			$html.="<tr> <td style='border: 0px;'> <b>Model: </b>$model</td> </tr>";
			$html.="<tr> <td style='border: 0px;'><b>Plate no: </b> $plateno </td> </tr>";
			$html.="<tr> <td style='border: 0px;'><b>Location ID: </b> $plot </td> </tr>";
			$html.="</table>";
		$html.="<div style='clear:both'></div>";
		$html.="<div id='customer'>";
            $html.="<table id='meta'>";
                $html.="<tr>";
                    $html.="<td class='meta-head'>Invoice #</td>";
                    $html.="<td>000123</td>";
                $html.="</tr>";
                $html.="<tr>";
                    $html.="<td class='meta-head'>Date</td>";
                    $html.="<td id='date'>December 15, 2009</td>";
                $html.="</tr>";
                $html.="<tr>";
                    $html.="<td class='meta-head'>Amount Due</td>";
                    $html.="<td><div class='due'>$amountpaid</div></td>";
                $html.="</tr>";
            $html.="</table>";
		$html.="</div>";
		$html.="<br>";
		$html.="<table style='width: 100%;'>";
		  $html.="<tr>";
		      $html.="<th style='background: #eee;'>Item</th>";
		      $html.="<th style='background: #eee;'>Description</th>";
		      $html.="<th style='background: #eee;'>Unit Cost</th>";
		      $html.="<th style='background: #eee;'>Quantity</th>";
		      $html.="<th style='background: #eee;'>Price</th>";
		  $html.="</tr>";
		  $html.="<tr class='item-row'>";
		      $html.="<td class='item-name'><div>Web Updates</div></td>";
		      $html.="<td class='description'>Parking for $vehicle !  $from - $to </td>
		      <td>$amountpaid</td>";
		      $html.="<td>1</td>";
		      $html.="<td>$amountpaid</td>";
		  $html.="</tr>";
		  $html.="<tr>";
		      $html.="<td colspan='2' class='blank' style='border-bottom: 0px;'></td>";
		      $html.="<td colspan='2' class='total-line'>Subtotal</td>";
		      $html.="<td class='total-value'><div id='subtotal'>$amountpaid</div></td>";
		  $html.="</tr>";
		  $html.="<tr>";
		      $html.="<td colspan='2' class='blank' > </td>";
		      $html.="<td colspan='2' class='total-line'>Total</td>";
		      $html.="<td class='total-value'><div id='total'>$amountpaid</div></td>";
		  $html.="</tr>";
		  $html.="<tr>";
		      $html.="<td colspan='2' class='blank'></td>";
		      $html.="<td colspan='2' class='total-line'>Amount Paid</td>";
		      $html.="<td class='total-value'>$amountpaid</td>";
		  $html.="</tr>";
		  $html.="<tr>";
		      $html.="<td colspan='2' class='blank'> </td>";
		      $html.="<td colspan='2' class='total-line balance'>Balance Due</td>";
		      $html.="<td class='total-value balance'><div class='due'>$0.00</div></td>";
		  $html.="</tr>";
		$html.="</table>";
	$html.="</div>";
$html.="</body>";
$html.="</html>";

$dompdf->loadHtml($html);

$dompdf->setPaper('A4', 'landscape');

$dompdf->render();

 $dompdf->stream();

?>