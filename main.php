<?php include 'functions.php';?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Car Parking</title>
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="css/mystyle.css">
</head>

<body>
<section class="container">
<header>
<section class="logo">
<img src="images/VU_Logo.png">
</section>
    <nav>
      <ul class="nav">
        <li><a href="index.php" class="icon home"><span>Home</span></a></li>
       
        <li class="dropdown">
          <a href="">Parking Zones</a>
          <ul class="large">
           <?php foreach($street as $key => $value){
		       	echo "<li><a href='region.php?id=".$key."'>".$value."</a></li>"; 
      	     } ?>
            <!--<li><a href="region-1.php">OGEMBO/ZONIC STREET - Cars Only</a></li>
            <li><a href="region-2.php">AGAKHAN STREET - Cars Only</a></li>
            <li><a href="region-3.php">DARAJA MBILI/UHURU PLAZA STREET -  Cars and Lorries</a></li>
            <li><a href="region-4.php">UCHUMI/RAM STREET - Lorries Only</a></li>
			<li><a href="region-5.php">NATIONAL BANK/LEVEL FIVE - Cars Only</a></li>
			<li><a href="region-6.php">MAIN MATATU/BUS STAGE  - PSV Only</a></li>-->
          </ul>
        </li>
		 
		<?php
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
   if( isset( $_SESSION['email'] ) )
   {
   echo  "<li><a href=\"your-car.php\">Reserve</a></li>";
   }
   else
   {
    echo "<li><a href=\"admin/index.php\">Admin Panel</a></li>";
   }
    ?>
     <li><a href="feedback.php">Feedback</a></li> 	
     <li><a href="contact.php">Contact</a></li>   
       
      </ul>
    </nav>
	<?php
	if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
   if( isset( $_SESSION['email'] ) )
      {
	   $name = $_SESSION["name"];
   echo "<p class=\"LogOut\">". $name. " "."<a href=\"process-log-out.php\">Log Out</a></p>";
   }
    
?>
	

  </header>
</section>
</body>

</html>